﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Loophole.Test
{
    // These test are probably useless
    [TestClass]
    public class TimeEventArgsTest
    {
        #region FrameTime
        [TestMethod]
        public void FrameTime_Returns50ms_If50msGetsPassedToTheConstructor()
        {
            var eventArgs = new TimeEventArgs(TimeSpan.FromMinutes(10), TimeSpan.FromMilliseconds(50));
            Assert.AreEqual(eventArgs.FrameTime, new TimeSpan(0, 0, 0, 0, 50));
        }

        [TestMethod]
        public void FrameTime_DoesNotReturnNull_OnDefault()
        {
            var eventArgs = new TimeEventArgs();
            Assert.IsNotNull(eventArgs.FrameTime, "TimeEventArgs.FrameTime is null");
        }

        [TestMethod]
        public void FrameTime_ReturnsZero_OnDefault()
        {
            var eventArgs = new TimeEventArgs();
            Assert.AreEqual(TimeSpan.Zero, eventArgs.FrameTime, "TimeEventArgs.FrameTime isn't Zero");
        }
        #endregion

        #region TotalTime
        [TestMethod]
        public void TotalTime_Returns10s_If10sGetsPassedToTheConstructor()
        {
            var eventArgs = new TimeEventArgs(TimeSpan.FromMinutes(10), TimeSpan.FromMilliseconds(50));
            Assert.AreEqual(eventArgs.TotalTime, new TimeSpan(0, 10, 0));
        }

        [TestMethod]
        public void TotalTime_DoesNotReturnNull_OnDefault()
        {
            var eventArgs = new TimeEventArgs();
            Assert.IsNotNull(eventArgs.TotalTime, "TimeEventArgs.TotalTime is null");
        }

        [TestMethod]
        public void TotalTime_ReturnsZero_OnDefault()
        {
            var eventArgs = new TimeEventArgs();
            Assert.AreEqual(TimeSpan.Zero, eventArgs.TotalTime, "TimeEventArgs.TotalTime isn't Zero");
        }
        #endregion
    }
}
