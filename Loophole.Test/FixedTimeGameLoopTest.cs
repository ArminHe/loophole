﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Loophole.Test
{
    [TestClass]
    public class FixedTimeGameLoopTest
    {
        FixedTimeGameLoop gameLoop30 = new FixedTimeGameLoop(30d);
        FixedTimeGameLoop gameLoop60 = new FixedTimeGameLoop(60d);
        FixedTimeGameLoop gameLoop120 = new FixedTimeGameLoop(120d);
        long ticksForOne200thOfASecond = 50000L;    //200fps
        long ticksForOne120thOfASecond = 83333L;    //120fps
        long ticksForOne60thOfASecond = 166667L;    //60fps
        long ticksForOne30thOfASecond = 333333L;    //30fps
        long ticksForOne18thOfASecond = 555556L;    //18fps

        #region Events fired
        [TestMethod]
        public void StartedEvent_GetsFired_AfterCallingStart()
        {
            bool fired = false;
            gameLoop60.Started += (o, e) => { fired = true; };
            gameLoop60.Update += (o, e) => { gameLoop60.Stop(); };
            gameLoop60.Start();
            Assert.AreEqual(true, fired);
        }

        [TestMethod]
        public void UpdateEvent_GetsFired_AfterCallingStart()
        {
            bool fired = false;
            gameLoop60.Update += (o, e) =>
            {
                fired = true;
                gameLoop60.Stop();
            };
            gameLoop60.Start();
            Assert.AreEqual(true, fired);
        }

        [TestMethod]
        public void DrawEvent_GetsFired_AfterCallingStart()
        {
            bool fired = false;
            gameLoop60.Draw += (o, e) => { fired = true; };
            gameLoop60.Update += (o, e) => { gameLoop60.Stop(); };
            gameLoop60.Start();
            Assert.AreEqual(true, fired);
        }

        [TestMethod]
        public void FinishedEvent_GetsFired_AfterCallingStop()
        {
            bool fired = false;
            gameLoop60.Update += (o, e) => { gameLoop60.Stop(); };
            gameLoop60.Finished += (o, e) => { fired = true; };
            gameLoop60.Start();
            Assert.AreEqual(true, fired);
        }
        #endregion

        #region IsRunning
        [TestMethod]
        public void IsRunning_IsFalse_BeforeRunIsCalled()
        {
            Assert.AreEqual(false, gameLoop60.IsRunning);
        }

        [TestMethod]
        public void IsRunning_IsTrue_AfterCallingStart()
        {
            bool isRunning = false;
            gameLoop60.Started += (o, e) => { isRunning = gameLoop60.IsRunning; };
            gameLoop60.Update += (o, e) => { gameLoop60.Stop(); };
            gameLoop60.Start();
            Assert.AreEqual(true, isRunning);
        }

        [TestMethod]
        public void UpdateEvent_DoesntGetsFired_AfterCallingStopInTheStartedEventHandler()
        {
            gameLoop60.Started += (o, e) => { gameLoop60.Stop(); };
            gameLoop60.Update += (o, e) => { throw new InvalidOperationException(); };
            gameLoop60.Start();
        }

        [TestMethod]
        public void IsRunning_IsTrue_WhileUpdateEventGetsFired()
        {
            bool isRunning = false;
            gameLoop60.Update += (o, e) =>
            {
                isRunning = gameLoop60.IsRunning;
                gameLoop60.Stop();
            };
            gameLoop60.Start();
            Assert.AreEqual(true, isRunning);
        }

        [TestMethod]
        public void IsRunning_IsFalse_AfterCallingStop()
        {
            gameLoop60.Stop();
            Assert.AreEqual(false, gameLoop60.IsRunning);
        }
        #endregion

        #region TargetFrameRate
        [TestMethod]
        public void TargetFrameRate_Returns120_When120WasPassedInTheConstructor()
        {
            Assert.AreEqual(120d, gameLoop120.TargetFrameRate);
        }

        [TestMethod]
        public void TargetFrameRate_Returns60_OnDefault()
        {
            Assert.AreEqual(60d, gameLoop60.TargetFrameRate);
        }

        [TestMethod]
        public void TargetFrameRate_Returns30_When30WasPassedInTheConstructor()
        {
            Assert.AreEqual(30d, gameLoop30.TargetFrameRate);
        }
        #endregion

        #region TargetMillisecondsPerFrame
        [TestMethod]
        public void TargetMillisecondsPerFrame_ReturnAbout8ms_For120FPS()
        {
            Assert.AreEqual(8.3333d, gameLoop120.TargetMillisecondsPerFrame, 0.0001d);
        }
        [TestMethod]
        public void TargetMillisecondsPerFrame_ReturnAbout16ms_For60FPS()
        {
            Assert.AreEqual(16.6666d, gameLoop60.TargetMillisecondsPerFrame, 0.0001d);
        }
        [TestMethod]
        public void TargetMillisecondsPerFrame_ReturnAbout33ms_For30FPS()
        {
            Assert.AreEqual(33.3333d, gameLoop30.TargetMillisecondsPerFrame, 0.0001d);
        }
        #endregion

        #region TargetSecondsPerFrame
        [TestMethod]
        public void TargetSecondsPerFrame_ReturnsAbout8ms_For120FPS()
        {
            Assert.AreEqual(0.0083333d, gameLoop120.TargetSecondsPerFrame, 0.0000001d);
        }
        [TestMethod]
        public void TargetSecondsPerFrame_ReturnsAbout16ms_For60FPS()
        {
            Assert.AreEqual(0.0166666d, gameLoop60.TargetSecondsPerFrame, 0.0000001d);
        }
        [TestMethod]
        public void TargetSecondsPerFrame_RetunsAbout33ms_For30FPS()
        {
            Assert.AreEqual(0.0333333d, gameLoop30.TargetSecondsPerFrame, 0.0000001d);
        }
        #endregion

        #region SettingTargetValues
        [TestMethod]
        public void TargetValues_ReturnCorrectValues_IfTargetFrameRateChanges()
        {
            gameLoop120.TargetFrameRate = 18d;
            Assert.AreEqual(18d, gameLoop120.TargetFrameRate, "TargetFrameRate");
            Assert.AreEqual(0.0555555d, gameLoop120.TargetSecondsPerFrame, 0.0000001d, "TargetSecondsPerFrame");
            Assert.AreEqual(55.5555d, gameLoop120.TargetMillisecondsPerFrame, 0.0001d, "TargetMillisecondsPerFrame");
        }

        [TestMethod]
        public void TargetValues_ReturnCorrectValues_IfTargetSecondsPerFrameChanges()
        {
            gameLoop120.TargetSecondsPerFrame = 0.05555555d;
            Assert.AreEqual(18d, gameLoop120.TargetFrameRate, 0.0001d, "TargetFrameRate");
            Assert.AreEqual(0.0555555d, gameLoop120.TargetSecondsPerFrame, 0.0000001d, "TargetSecondsPerFrame");
            Assert.AreEqual(55.5555d, gameLoop120.TargetMillisecondsPerFrame, 0.0001d, "TargetMillisecondsPerFrame");
        }

        [TestMethod]
        public void TargetValues_ReturnCorrectValues_IfTargetMillisecondsPerFrameChanges()
        {
            gameLoop120.TargetMillisecondsPerFrame = 55.5555d;
            Assert.AreEqual(18d, gameLoop120.TargetFrameRate, 0.0001d, "TargetFrameRate");
            Assert.AreEqual(0.0555555d, gameLoop120.TargetSecondsPerFrame, 0.0000001d, "TargetSecondsPerFrame");
            Assert.AreEqual(55.5555d, gameLoop120.TargetMillisecondsPerFrame, 0.0001d, "TargetMillisecondsPerFrame");
        }
        #endregion

        #region TimeEventArgs
        [TestMethod]
        public void UpdateEvent_ContainsTimeEventArgs_IfUpdateEventGetsFired()
        {
            bool correctType = false;
            gameLoop60.Update += (o, e) =>
            {
                correctType = (e.GetType() == typeof(TimeEventArgs));
                gameLoop60.Stop();
            };
            gameLoop60.Start();
            Assert.AreEqual(true, correctType);
        }
        #endregion

        #region Timing
        //120fps
        [TestMethod]
        public void UpdateEvent_ReturnsFrameTimeOfOne120thOfASecond_OnA120FPSGameLoop()
        {
            TimeSpan time = TimeSpan.Zero;
            gameLoop120.Update += (o, e) =>
            {
                time = e.FrameTime;
                gameLoop120.Stop();
            };
            gameLoop120.Start();
            Assert.AreEqual(TimeSpan.FromTicks(ticksForOne120thOfASecond).TotalSeconds, time.TotalSeconds, "GameLoop has not a FrameTime of 8.3 ms");
        }

        //60fps
        [TestMethod]
        public void UpdateEvent_ReturnsFrameTimeOfOne60thOfASecond_OnA60FPSGameLoop()
        {
            TimeSpan time = TimeSpan.Zero;
            gameLoop60.Update += (o, e) =>
            {
                time = e.FrameTime;
                gameLoop60.Stop();
            };
            gameLoop60.Start();
            Assert.AreEqual(TimeSpan.FromTicks(ticksForOne60thOfASecond).TotalSeconds, time.TotalSeconds, "Default GameLoop has not  a FrameTime of 16.6 ms");
        }

        //60fps
        [TestMethod]
        public void UpdateEvent_ReturnsTotalTimeOfOneSecond_OnA60FPSGameLoop()
        {
            TimeSpan time = TimeSpan.Zero;
            gameLoop60.Update += (o, e) =>
            {
                time = e.TotalTime;
                gameLoop60.Stop();
            };
            gameLoop60.Start();
            Assert.AreEqual(TimeSpan.FromTicks(ticksForOne60thOfASecond).TotalSeconds, time.TotalSeconds, "Default GameLoop has not a TotalTime of 16.6 ms for the first frame");
        }

        //60fps
        [TestMethod]
        public void UpdateEvent_RetunssTotalTimeOfOneSecondsFor60Frame_OnA60FPSGameLoop()
        {
            TimeSpan time = TimeSpan.Zero;
            int count = 0;
            gameLoop60.Update += (o, e) =>
            {
                count++;
                if (count == 60)
                {
                    time = e.TotalTime;
                    gameLoop60.Stop();
                }
            };
            gameLoop60.Start();
            Assert.AreEqual(TimeSpan.FromTicks(ticksForOne60thOfASecond * 60).TotalSeconds, time.TotalSeconds, 0.000003d);
        }

        //30fps
        [TestMethod]
        public void UpdateEvent_ReturnsFrameTimeOfOne30thOfASecond_OnA30FPSGameLoop()
        {
            TimeSpan time = TimeSpan.Zero;
            gameLoop30.Update += (o, e) =>
            {
                time = e.FrameTime;
                gameLoop30.Stop();
            };
            gameLoop30.Start();
            Assert.AreEqual(TimeSpan.FromTicks(ticksForOne30thOfASecond).TotalSeconds, time.TotalSeconds, "GameLoop has not a FrameTime of 33.3 ms");
        }
        #endregion

        #region FrameRateToTicks
        [TestMethod]
        public void FrameRateToTicks_Returns50000_If200GetsPassedIntoIt()
        {
            Assert.AreEqual(ticksForOne200thOfASecond, FixedTimeGameLoop.FrameRateToTicks(200d));
        }

        [TestMethod]
        public void FrameRateToTicks_Returns83333_If120GetsPassedIntoIt()
        {
            Assert.AreEqual(ticksForOne120thOfASecond, FixedTimeGameLoop.FrameRateToTicks(120d));
        }

        [TestMethod]
        public void FrameRateToTicks_Returns166667_If60GetsPassedIntoIt()
        {
            Assert.AreEqual(ticksForOne60thOfASecond, FixedTimeGameLoop.FrameRateToTicks(60d));
        }

        [TestMethod]
        public void FrameRateToTicks_Returns333333_If30GetsPassedIntoIt()
        {
            Assert.AreEqual(ticksForOne30thOfASecond, FixedTimeGameLoop.FrameRateToTicks(30d));
        }

        [TestMethod]
        public void FrameRateToTicks_Returns555556_If18GetsPassedIntoIt()  //important test
        {
            Assert.AreEqual(ticksForOne18thOfASecond, FixedTimeGameLoop.FrameRateToTicks(18d));
        }
        #endregion

        #region Update and Draw Count
        [TestMethod]
        public void UpdateCount_RetunsZero_BeforeCallingStart()
        {
            Assert.AreEqual(0U, gameLoop30.UpdateCount, "UpdateCount");
        }

        [TestMethod]
        public void UpdateCount_RetunsOne_AfterCallingStart()
        {
            gameLoop30.Update += (o, e) =>
            {
                gameLoop30.Stop();
            };
            gameLoop30.Start();

            Assert.AreEqual(1U, gameLoop30.UpdateCount, "UpdateCount");
        }

        [TestMethod]
        public void DrawCount_RetunsZero_BeforeCallingStart()
        {
            Assert.AreEqual(0U, gameLoop30.DrawCount, "DrawCount");
        }

        [TestMethod]
        public void DrawCount_RetunsOne_AfterCallingStart()
        {
            gameLoop30.Update += (o, e) =>
            {
                gameLoop30.Stop();
            };
            gameLoop30.Start();

            Assert.AreEqual(1U, gameLoop30.DrawCount, "DrawCount");
        }

        //I dont think I need more for this
        #endregion

        #region Clamp FrameRate 
        [TestMethod]
        public void TargetFrameRate_Returns200_IfANumberHigherThanTheMaxFrameRateGetsPassedIntoTheConstructor()
        {
            var loop = new FixedTimeGameLoop(500d);
            Assert.AreEqual(200d, loop.TargetFrameRate);
        }

        [TestMethod]
        public void TargetFrameRate_Returns1_IfANumberLowerThanTheMinFrameRateGetsPassedIntoTheConstructor()
        {
            var loop = new FixedTimeGameLoop(0.15d);
            Assert.AreEqual(1d, loop.TargetFrameRate);
        }

        [TestMethod]
        public void TargetFrameRate_Returns1_IfANegativeNumberGetsPassedIntoTheConstructor()
        {
            var loop = new FixedTimeGameLoop(-60d);
            Assert.AreEqual(1d, loop.TargetFrameRate);
        }

        [TestMethod]
        public void TargetFrameRate_ReturnsThePassedNumber_IfANumberBetweenMinAndMaxFrameRateGetsPassedIntoTheConstructor()
        {
            var loop = new FixedTimeGameLoop(81d);
            Assert.AreEqual(81d, loop.TargetFrameRate);
        }
        #endregion
    }
}
