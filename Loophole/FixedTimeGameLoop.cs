﻿using System;
using System.Diagnostics;

namespace Loophole
{
    public class FixedTimeGameLoop
    {
        #region Fields
        private const long ticksPerMillisecond = TimeSpan.TicksPerMillisecond;  // 10.000 Ticks
        private const long maximalFrameTimeInTicks = ticksPerMillisecond * 500; // 0.5 seconds

        private const double minFrameRate = 1d;
        private const double maxFrameRate = 200d;

        private Stopwatch stopwatch;
        private bool running = false;

        private uint updateCount = 0;
        private uint drawCount = 0;

        private double targetFrameRate;
        private TimeSpan targetFrameTime;
        private long targetFrameTimeInTicks;

        private long totalGameTimeInTicks = 0;
        private long previousTimeInTicks = 0;
        private long accumulatedTicks = 0;
        #endregion

        #region Properties
        public bool IsRunning
        {
            get { return running; }
        }

        public double TargetFrameRate
        {
            get { return targetFrameRate; }
            set { SetTargetValues(value); }
        }

        public double TargetSecondsPerFrame
        {
            get { return 1d / targetFrameRate; }
            set { SetTargetValues(1d / value); }
        }

        public double TargetMillisecondsPerFrame
        {
            get { return 1000d / targetFrameRate; }
            set { SetTargetValues(1000d / value); }
        }

        public double MinFrameRate
        {
            get { return minFrameRate; }
        }

        public double MaxFrameRate
        {
            get { return maxFrameRate; }
        }

        public uint UpdateCount
        {
            get { return updateCount; }
        }

        public uint DrawCount
        {
            get { return drawCount; }
        }
        #endregion

        #region Constructor
        public FixedTimeGameLoop(double frameRate = 60d)
        {
            SetTargetValues(frameRate);
        }
        #endregion

        #region Methods
        public void Start()
        {
            running = true;
            bool updateAccumulator = false;

            OnStarted();

            stopwatch = Stopwatch.StartNew();
            while (running)
            {
                // This while updates the accumulator and checks if we need to sleep,
                // if this is the case we loop and update the accumulator again.
                updateAccumulator = true;
                while (updateAccumulator)
                {
                    long currentTimeInTicks = stopwatch.Elapsed.Ticks;
                    accumulatedTicks += currentTimeInTicks - previousTimeInTicks;
                    previousTimeInTicks = currentTimeInTicks;
                    updateAccumulator = false;

                    // We need to wait if it is not time for the next update event yet.
                    if (accumulatedTicks < targetFrameTimeInTicks)
                    {
                        int sleepTime = (int)((targetFrameTimeInTicks - accumulatedTicks) / ticksPerMillisecond);
                        Sleep(sleepTime);
                        updateAccumulator = true;
                    }
                }

                // If the game or program took longer than the normal frameTime,
                // clamp the accumulator, so we only catch up a maximum of 0.5 seconds.
                if (accumulatedTicks > maximalFrameTimeInTicks)
                    accumulatedTicks = maximalFrameTimeInTicks;

                // Fire the update event as often as the accumulator allows.
                while (accumulatedTicks >= targetFrameTimeInTicks)
                {
                    accumulatedTicks -= targetFrameTimeInTicks;
                    totalGameTimeInTicks += targetFrameTimeInTicks;

                    OnUpdate(TimeSpan.FromTicks(totalGameTimeInTicks), targetFrameTime);
                }
                OnDraw();
            }
            // End of the loop, stop the timer and call finished.
            stopwatch.Stop();

            OnFinished();
        }

        public void Stop()
        {
            running = false;
        }

        private void Sleep(int sleepTime)
        {
#if NET45
            System.Threading.Tasks.Task.Delay(sleepTime).Wait(); //.NET 4.5
#else
            System.Threading.Thread.Sleep(sleepTime);  //.NET 3.5
#endif
        }

        private void SetTargetValues(double frameRate)
        {
            frameRate = ClampFrameRate(frameRate);

            targetFrameRate = frameRate;
            targetFrameTimeInTicks = FrameRateToTicks(targetFrameRate);
            targetFrameTime = TimeSpan.FromTicks(targetFrameTimeInTicks);
        }

        private double ClampFrameRate(double frameRate)
        {
            if (frameRate > maxFrameRate)
                return maxFrameRate;
            if (frameRate < minFrameRate)
                return minFrameRate;

            return frameRate;
        }

        public static long FrameRateToTicks(double frameRate)
        {
            return (long)Math.Round((double)(ticksPerMillisecond * 1000) / frameRate);
        }
        #endregion

        #region Events
        public event EventHandler<EventArgs> Started;
        public event EventHandler<TimeEventArgs> Update;
        public event EventHandler<EventArgs> Draw;
        public event EventHandler<EventArgs> Finished;

        protected void OnStarted()
        {
            if (Started != null)
                Started(this, EventArgs.Empty);
        }

        protected void OnUpdate(TimeSpan totalTime, TimeSpan frameTime)
        {
            ++updateCount;

            if (Update != null)
                Update(this, new TimeEventArgs(totalTime, frameTime));
        }

        protected void OnDraw()
        {
            ++drawCount;

            if (Draw != null)
                Draw(this, EventArgs.Empty);
        }

        protected void OnFinished()
        {
            if (Finished != null)
                Finished(this, EventArgs.Empty);
        }
        #endregion
    }
}
