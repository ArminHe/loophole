﻿using System;

namespace Loophole
{
    public class TimeEventArgs : EventArgs
    {
        public TimeSpan TotalTime { get; set; }
        public TimeSpan FrameTime { get; set; }

        public TimeEventArgs()
        {
            TotalTime = TimeSpan.Zero;
            FrameTime = TimeSpan.Zero;
        }

        public TimeEventArgs(TimeSpan totalTime, TimeSpan frameTime)
        {
            TotalTime = totalTime;
            FrameTime = frameTime;
        }
    }
}
