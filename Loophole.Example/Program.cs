﻿using System;

namespace Loophole.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            Game myCoolGame = new Game();
            myCoolGame.Start();
        }
    }
}
