﻿using System;

namespace Loophole.Example
{
    public class Game
    {
        private FixedTimeGameLoop gameloop;

        public Game()
        {
            gameloop = new FixedTimeGameLoop(30);

            gameloop.Started += this.Initialize;
            gameloop.Update += this.Update;
            gameloop.Draw += this.Draw;
            gameloop.Finished += this.Unload;
        }

        private void Initialize(object sender, EventArgs e)
        {
            // Instatiate all your objects and load your graphics here.
        }

        private void Update(object sender, TimeEventArgs e)
        {
            // Handle input and update the gamelogic here.
            Console.WriteLine("UpdateCount: {2,3} - FrameTime: {0} - TotalTime: {1}", e.FrameTime.TotalSeconds, e.TotalTime.TotalSeconds, gameloop.UpdateCount);

            // Call the Stop method whenever you decide to end the game
            if (gameloop.UpdateCount >= gameloop.TargetFrameRate * 4) // 4 seconds passed, close the game
            {
                gameloop.Stop();
            }
        }

        private void Draw(object sender, EventArgs e)
        {
            // Draw your pretty graphics here.
        }

        private void Unload(object sender, EventArgs e)
        {
            // Destroy your objects, unload your graphics and save important data to disc here.
        }

        public void Start()
        {
            gameloop.Start();
        }
    }
}
